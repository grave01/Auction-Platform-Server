CREATE DATABASE IF NOT EXISTS auction_platform
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_0900_ai_ci;

USE auction_platform;

CREATE TABLE IF NOT EXISTS `auction_platform`.`tb_quartz_job`  (
  `job_id` int NOT NULL AUTO_INCREMENT COMMENT '定时任务唯一标志ID',
  `job_name` varchar(50) NOT NULL COMMENT '定时任务名称',
  `target_class_name` varchar(50),
  `cron_expression` varchar(50),
  `job_status` varchar(50) COMMENT '任务状态',
  `job_desc` varchar(50),
  `create_time` datetime COMMENT '创建时间',
  `creater_name` varchar(50),
  `update_time` datetime COMMENT '更新时间',
  PRIMARY KEY (`job_id`)
);
alter table `tb_quartz_job` AUTO_INCREMENT=1;