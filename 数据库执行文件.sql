CREATE DATABASE IF NOT EXISTS auction_platform
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_0900_ai_ci;

USE auction_platform;

CREATE TABLE IF NOT EXISTS `auction_platform`.`tb_user`  (
  `uid` int NOT NULL AUTO_INCREMENT COMMENT '用户唯一标志ID',
  `name` varchar(16) NOT NULL COMMENT '用户昵称' check(CHAR_LENGTH( `name` ) >=1 AND CHAR_LENGTH( `name` ) <=16),
  `password` varchar(16) NOT NULL COMMENT '用户密码' check(CHAR_LENGTH( `password` ) >=6 AND CHAR_LENGTH( `password` ) <=16),
  `pay_password` varchar(6) NOT NULL COMMENT '用户支付密码' check(CHAR_LENGTH( `pay_password` ) = 6),
  `phone` char(11) NOT NULL COMMENT '用户电话' check(CHAR_LENGTH( `phone` ) = 11),
  `email` varchar(20) NULL COMMENT '用户邮箱',
  `gender` int NULL DEFAULT -1 COMMENT '性别',
  `qq` varchar(11) NULL COMMENT 'QQ',
  `photo` varchar(20) NULL COMMENT '头像',
  PRIMARY KEY (`uid`),
  UNIQUE INDEX `phone`(`phone`) USING BTREE COMMENT 'phone唯一',
  UNIQUE INDEX `email`(`email`) USING BTREE COMMENT 'email唯一',
  UNIQUE INDEX `qq`(`qq`) USING BTREE COMMENT 'qq唯一'
);
alter table `tb_user` AUTO_INCREMENT=10000;

CREATE TABLE IF NOT EXISTS `auction_platform`.`tb_lot`  (
  `lot_id` int NOT NULL AUTO_INCREMENT COMMENT '拍品唯一标志ID',
  `type` int NOT NULL COMMENT '拍品类型',
  `title` varchar(15) NOT NULL check(CHAR_LENGTH(`title` ) >=5),
  `info` varchar(50) NOT NULL check(CHAR_LENGTH(`info` ) >=5),
  `date` datetime NOT NULL COMMENT '发布时间，带秒',
  `end_date` datetime NOT NULL COMMENT '拍卖结束时间，带小时',
  `start_price` Decimal(9,2) NOT NULL COMMENT '起拍价',
  `markup_range` Decimal(9,2) NOT NULL COMMENT '加价幅度',
  `images` varchar(50) NULL,
  `tag` int NOT NULL DEFAULT 1 COMMENT '0(已拍卖)/1(正在拍卖)/2(流拍)',
  `look` int DEFAULT 0 NOT NULL COMMENT '浏览量',
  `auction_uid` int NULL COMMENT '拍得的用户',
  `uid` int NOT NULL,
  PRIMARY KEY (`lot_id`),
  CONSTRAINT `uid` FOREIGN KEY (`uid`) REFERENCES `auction_platform`.`tb_user` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
);
alter table `tb_lot` AUTO_INCREMENT=1000;

CREATE TABLE IF NOT EXISTS `auction_platform`.`tb_lot_record`  (
  `r_id` int NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL COMMENT '拍卖时间',
  `price` Decimal(9,2) NOT NULL COMMENT '拍卖的价格',
  `uid` int NOT NULL,
  `lot_id` int NOT NULL,
  PRIMARY KEY (`r_id`),
  CONSTRAINT `record_uid` FOREIGN KEY (`uid`) REFERENCES `auction_platform`.`tb_user` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `record_lot_id` FOREIGN KEY (`lot_id`) REFERENCES `auction_platform`.`tb_lot` (`lot_id`) ON DELETE CASCADE ON UPDATE CASCADE
);
alter table `tb_lot_record` AUTO_INCREMENT=1000;

CREATE TABLE IF NOT EXISTS `auction_platform`.`tb_collection`  (
  `c_id` int NOT NULL AUTO_INCREMENT,
  `uid` int NOT NULL,
  `lot_id` int NOT NULL,
  PRIMARY KEY (`c_id`),
  CONSTRAINT `collection_uid` FOREIGN KEY (`uid`) REFERENCES `auction_platform`.`tb_user` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `collection_lot_id` FOREIGN KEY (`lot_id`) REFERENCES `auction_platform`.`tb_lot` (`lot_id`) ON DELETE CASCADE ON UPDATE CASCADE
);