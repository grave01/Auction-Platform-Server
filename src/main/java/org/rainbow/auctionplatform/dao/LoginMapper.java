package org.rainbow.auctionplatform.dao;

import org.apache.ibatis.annotations.Param;
import org.rainbow.auctionplatform.bean.User;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginMapper {
    // 查找用户
    User selectUser(@Param("account") String account, @Param("password") String password);
}
