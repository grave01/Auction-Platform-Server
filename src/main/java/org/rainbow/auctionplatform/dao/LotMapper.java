package org.rainbow.auctionplatform.dao;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.ibatis.annotations.Param;
import org.rainbow.auctionplatform.bean.LotRecord;
import org.rainbow.auctionplatform.pojo.HomeSliderData;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.rainbow.auctionplatform.pojo.LotDetailData;
import org.rainbow.auctionplatform.pojo.LotRecordData;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LotMapper {

    // 通过lotId得到拍品信息
    LotDetailData getLotById(Integer lotId);

    // 通过lotId得到拍品记录数量
    Integer getLotRecordNum(Integer lotId);

    // 查询是否收藏
    Boolean getIsCollection(@Param("uid") Integer uid, @Param("lotId") Integer lotId);

    // 插入收藏
    Boolean insertLotCollection(@Param("uid") Integer uid, @Param("lotId") Integer lotId);

    // 删除收藏
    Boolean deleteLotCollection(@Param("uid") Integer uid, @Param("lotId") Integer lotId);

    // 通过lotId得到拍品记录
    List<LotRecordData> getLotRecord(@Param("lotId") Integer lotId, @Param("lastRId") Integer lastRId, @Param("pageSize") Integer pageSize);

    // 得到轮播图信息
    List<HomeSliderData> getHomeSlider();

    // 得到失物招领卡片信息(涉及瀑布流)
    List<LotCardData> getLotCards(@Param("type") Integer type, @Param("lastLotId") Integer lastLotId, @Param("pageSize") Integer pageSize);

    // 查询支付密码
    String selectPayPassword(Integer uid);

    // 得到最后一条拍卖记录
    LotRecord getLastLotRecord(Integer lotId);

    // 更新拍卖价格
    Boolean updatePrice(@Param("lotId") Integer lotId, @Param("uid")  Integer uid);

    // 插入拍卖记录
    Boolean insertLotRecord(LotRecord lotRecord);

    // 更新浏览量
    void updateLook(Integer lotId);

    // 查询拍品拍得人
    String selectAuctionUid(Integer lotId);

    // 更新拍卖标识
    void updateTag(@Param("lotId") Integer lotId, @Param("tag") Integer tag);

    // 得到我的拍卖
    List<LotCardData> getMyLotCards(@Param("uid") Integer uid, @Param("lastLotId") Integer lastLotId, @Param("pageSize") Integer pageSize);

    //得到我的收藏
    List<LotCardData> getMyCollectionLotCards(@Param("uid") Integer uid, @Param("lastCId") Integer lastCId, @Param("pageSize") Integer pageSize);

    //得到参与的拍卖
    List<LotCardData> getParticipatingAuctionsLotCards(@Param("uid") Integer uid, @Param("lastLotId") Integer lastLotId, @Param("pageSize") Integer pageSize);

    // 搜索
    List<LotCardData> search(@Param("keyWord") String keyWord, @Param("lastLotId") Integer lastLotId, @Param("pageSize") Integer pageSize);

    // 查询最大id
    int selectMaxUid();
    // 重置自增id缓存
    void resetMaxUid(int maxId);
}
