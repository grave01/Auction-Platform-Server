package org.rainbow.auctionplatform.dao;

import org.rainbow.auctionplatform.model.QuartzJobModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuartzJobMapper {
    // 得到所有定时任务
    List<QuartzJobModel> getAllJobs();

    // 保存任务到数据库
    Boolean addJob(QuartzJobModel jobModel);

    // 删除数据库里的定时任务
    Boolean deleteJob(String jobName);
}
