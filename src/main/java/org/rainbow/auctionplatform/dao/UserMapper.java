package org.rainbow.auctionplatform.dao;

import org.apache.ibatis.annotations.Param;
import org.rainbow.auctionplatform.bean.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    // 通过uid查找用户
    User selectUserByUid(Integer uid);

    // 通过uid查找用户和密码
    User selectUserAndPwByUid(Integer uid);

    // 用户修改(不包括密码)
    Boolean updateUser(User user);

    // 修改头像
    Boolean updateUserPhoto(@Param("uid") Integer uid, @Param("photo") String photo);

    // 修改用户登录密码
    Boolean updatePassword(@Param("uid") Integer uid, @Param("password") String password);

    // 修改用户支付密码
    Boolean updatePayPassword(@Param("uid") Integer uid, @Param("payPassword") String payPassword);
}
