package org.rainbow.auctionplatform.dao;

import org.rainbow.auctionplatform.bean.User;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterMapper {
    // 插入用户
    boolean insertUser(User user);
    // 查询最大id
    int selectMaxUid();
    // 重置自增id缓存
    void resetMaxUid(int maxId);
}
