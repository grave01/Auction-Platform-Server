package org.rainbow.auctionplatform.dao;

import org.apache.ibatis.annotations.Param;
import org.rainbow.auctionplatform.bean.Lot;
import org.springframework.stereotype.Repository;

@Repository
public interface AddMapper {
    // 插入失物招领消息
    boolean insertLot(Lot lot);

    // 更新失物招领图片地址
    boolean updateImages(@Param("lotId") Integer lotId, @Param("images") String images);

    // 查询最大id
    int selectMaxLFid();
    // 重置自增id缓存
    void resetMaxLFid(int maxId);
}
