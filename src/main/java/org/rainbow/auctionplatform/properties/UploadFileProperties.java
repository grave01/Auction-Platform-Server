package org.rainbow.auctionplatform.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 上传文件路径配置类
 */

@Configuration
@PropertySource("classpath:uploadFilePath.properties")  //属性文件需放在根目录的resources文件夹下面，才能被读取出来
public class UploadFileProperties {
    // 图片上传路径
    @Value("${imagePath}")
    private String imagePath;
    // 头像上传路径
    @Value("${photoPath}")
    private String photoPath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }
}
