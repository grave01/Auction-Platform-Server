package org.rainbow.auctionplatform.model;

import java.util.Date;
// 定时任务model
public class QuartzJobModel {
    private String jobId;
    private String jobName;
    private String targetClassName;
    private String cronExpression;
    private String jobStatus;
    private String jobDesc;
    private Date createTime;
    private String createrName;
    private Date updateTime;

    public QuartzJobModel() {
    }

    public QuartzJobModel(String jobId, String jobName, String targetClassName, String cronExpression, String jobStatus, String jobDesc, Date createTime, String createrName, Date updateTime) {
        this.jobId = jobId;
        this.jobName = jobName;
        this.targetClassName = targetClassName;
        this.cronExpression = cronExpression;
        this.jobStatus = jobStatus;
        this.jobDesc = jobDesc;
        this.createTime = createTime;
        this.createrName = createrName;
        this.updateTime = updateTime;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobDesc() {
        return jobDesc;
    }

    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreaterName() {
        return createrName;
    }

    public void setCreaterName(String createrName) {
        this.createrName = createrName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "QuartzJobModel{" +
                "jobId='" + jobId + '\'' +
                ", jobName='" + jobName + '\'' +
                ", targetClassName='" + targetClassName + '\'' +
                ", cronExpression='" + cronExpression + '\'' +
                ", jobStatus='" + jobStatus + '\'' +
                ", jobDesc='" + jobDesc + '\'' +
                ", createTime=" + createTime +
                ", createrName='" + createrName + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}
