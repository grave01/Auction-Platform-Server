package org.rainbow.auctionplatform.utils;

import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 图片压缩工具类
 */
public class ImageCompress {

    public final static int IMAGE_COMPRESS_SUCCESS = 0;
    public final static int IMAGE_ERROR = -1;
    public final static int IMAGE_NO_FILE_ERROR = -100;

    /**
     * 图片压缩 Google大法 因为Thumbnails.of() 方法是一个重载方法，参数不仅仅局限于是一个文件类型 可以是以流的形式 File形式，ImageBuffer对象，URL路径,String类型指定文件路径
     * 然后可通过链式构造器传入不通参数，压缩比例，指定输出的格式等最终通过toFile("文件存储路径")返回一个已经压缩完成的图片。
     * @param image 待压缩的图片
     * @param fileName 压缩后的图片名
     * @param realPath 图片保存路径
     * @return 信息
     */
    public static int compressFile(MultipartFile image, String fileName, String realPath) {
        if (image.isEmpty()) {
            return IMAGE_NO_FILE_ERROR;
        }

        File folder = new File(realPath);
        // 若文件夹不存在，则创建
        if (!folder.isDirectory()) {
            folder.mkdirs();
        }

        //得到上传时的原文件名
        String originalFilename = image.getOriginalFilename();
        //获取文件格式
        String ext = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
        //路径+文件名
        String path = realPath + fileName;
        try {
            // 先尝试压缩并保存图片
            Thumbnails.of(image.getInputStream())
                    .scale(1f)
                    .outputQuality(0.25f)
                    .outputFormat(ext)
                    .toFile(path);
        } catch (IOException e) {
            e.printStackTrace();
            return IMAGE_ERROR;
        }
        return IMAGE_COMPRESS_SUCCESS;
    }
}
