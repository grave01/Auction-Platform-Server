package org.rainbow.auctionplatform.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式化工具
 */
public class DateUtil {

    public static final String MYSQL_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final String MYSQL_DATE = "yyyy-MM-dd";

    /**
     * 按照约定格式格式化日期，返回其字符串
     * @param date 日期
     * @param format 格式 yyyy-MM-dd
     * @return String
     */
    public static String dateToString(Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }
}
