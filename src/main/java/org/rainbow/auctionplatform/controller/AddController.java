package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.bean.Lot;
import org.rainbow.auctionplatform.service.AddService;
import org.rainbow.auctionplatform.utils.ImageCompress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.Objects;

@RestController
public class AddController {

    private final AddService addService;

    private Lot lot;

    @Autowired
    public AddController(AddService addService) {
        this.addService = addService;
    }

    /**
     * 拍品信息发布
     * @param lot 拍品信息
     * @return 错误信息
     */
    @PostMapping("/lot")
    public Data<Lot> addInfo(@RequestBody Lot lot) {
        System.out.println(lot);
        Data<Lot> addData;
        if (lot != null) {
            this.lot = lot;
            int addInfo = addService.insertLot(lot);
            switch (addInfo) {
                case AddService.INSERT_SUCCESS:
                    addData = new Data<>(200, "ok", Collections.singletonList(lot));
                    break;
                case AddService.INSERT_ERROR:
                    addData = new Data<>(200, "拍品发布失败，请稍后再试", null);
                    break;
                case AddService.INSERT_UID_NULL:
                    addData = new Data<>(200, "用户uid不能为空", null);
                    break;
                default:
                    addData = new Data<>(200, "拍品发布错误，请稍后再试", null);
                    break;
            }
            return addData;
        }
        addData = new Data<>(200, "拍品数据异常，请稍后再试", null);
        return addData;
    }

    /**
     * 拍品图片上传
     * @param file 拍品图片文件
     * @param imageNum 图片标识
     * @return 图片名
     */
    @PostMapping("/lot/image")
    public String addImage(@RequestParam("file") MultipartFile file, @RequestParam("imageNum") String imageNum) {
        int uploadInfo = addService.uploadImage(file, imageNum, lot.getLotId());
        String filename = "";
        switch (uploadInfo) {
            case ImageCompress.IMAGE_COMPRESS_SUCCESS:
                //得到上传时的原文件名
                String originalFilename = file.getOriginalFilename();
                //获取文件格式
                String ext = Objects.requireNonNull(originalFilename).substring(originalFilename.lastIndexOf(".") + 1);
                filename = lot.getLotId() + "-" + imageNum + "." + ext;
                break;
            case ImageCompress.IMAGE_NO_FILE_ERROR:
                break;
            case ImageCompress.IMAGE_ERROR:
                break;
            default:
                break;
        }
        return filename;
    }

    /**
     * 拍品图片地址上传
     * @param images 图片地址
     * @return 错误信息
     */
    @GetMapping("/lot/images")
    public Data<Lot> updateImages(String images) {
        int update_flag = addService.updateImages(lot.getLotId(), images);
        Data<Lot> updateInfo;
        switch (update_flag) {
            case AddService.UPDATE_SUCCESS:
                lot.setImages(images);
                updateInfo = new Data<>(200, "ok", Collections.singletonList(lot));
                break;
            case AddService.UPDATE_ERROR:
                updateInfo = new Data<>(200, "图片上传出错，请稍后重试", null);
                break;
            default:
                updateInfo = new Data<>(200, "图片数据出错，请稍后重试", null);
                break;
        }
        return updateInfo;
    }
}
