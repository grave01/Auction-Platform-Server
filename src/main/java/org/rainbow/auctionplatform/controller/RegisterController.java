package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.bean.User;
import org.rainbow.auctionplatform.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class RegisterController {

    private final RegisterService registerService;

    @Autowired
    public RegisterController(RegisterService registerService) {
        this.registerService = registerService;
    }

    @PostMapping("/register")
    public Data<User> register(@RequestBody User user) {
        Data<User> userData;
        System.out.println(user);
        if (user != null) {
            // 获取注册后返回的执行代码
            int registerInfo = registerService.insertUser(user);
            // 通过执行代码处理返回数据
            switch (registerInfo) {
                case RegisterService.INSERT_SUCCESS:
                    userData = new Data<>(200, "ok", Collections.singletonList(user));
                    break;
                case RegisterService.INSERT_PHONE_DUPLICATE:
                    userData = new Data<>(200, "该手机号已被注册，请登录", null);
                    break;
                case RegisterService.INSERT_EMAIL_DUPLICATE:
                    userData = new Data<>(200, "该邮箱已被注册，请登录", null);
                    break;
                case RegisterService.INSERT_QQ_DUPLICATE:
                    userData = new Data<>(200, "该QQ已被注册，请登录", null);
                    break;
                default:
                    userData = new Data<>(200, "注册错误，请稍后重试", null);
                    break;
            }
            return userData;
        }
        userData = new Data<>(200, "注册数据异常，请稍后再试", null);
        return userData;
    }
}
