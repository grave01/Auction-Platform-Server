package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.bean.LotRecord;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.rainbow.auctionplatform.pojo.LotDetailData;
import org.rainbow.auctionplatform.pojo.LotRecordData;
import org.rainbow.auctionplatform.service.LotDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
public class LotDetailController {

    private final LotDetailService lotDetailService;

    @Autowired
    public LotDetailController(LotDetailService lotDetailService) {
        this.lotDetailService = lotDetailService;
    }

    /**
     * 通过lotId得到拍品详情信息
     */
    @GetMapping("/lot/{lotId}")
    public Data<LotDetailData> getLotDetailData(@PathVariable(name = "lotId") Integer lotId) {
        LotDetailData lotDetailData = lotDetailService.getLotById(lotId);
        if (lotDetailData != null) {
            return new Data<>(200, "success", Collections.singletonList(lotDetailData));
        } else {
            return new Data<>(200, "未知拍品id", null);
        }
    }

    /**
     * 通过uid和lotId得到用户是否收藏该拍品
     */
    @GetMapping("/getIsCollection")
    public Data<Boolean> getIsCollection(Integer uid, Integer lotId) {
        if (uid == null || lotId == null) {
            return new Data<>(200, "未知id", null);
        }
        Boolean isCollection = lotDetailService.getIsCollection(uid, lotId);
        if (isCollection != null && isCollection) {
            return new Data<>(200, "success", Collections.singletonList(true));
        } else {
            return new Data<>(200, "success", Collections.singletonList(false));
        }
    }

    // 收藏
    @PostMapping("/lotCollection")
    public Data<String> insertLotCollection(@RequestBody Map<String, Integer> map) {
        Integer uid = map.get("uid");
        Integer lotId = map.get("lotId");
        if (uid == null || lotId == null) {
            return new Data<>(200, "未知id", null);
        }
        if (lotDetailService.insertLotCollection(uid, lotId)) {
            return new Data<>(200, "success", null);
        } else {
            return new Data<>(200, "收藏失败，请稍后再试", null);
        }
    }

    // 取消收藏
    @DeleteMapping("/lotCollection")
    public Data<String> deleteLotCollection(@RequestBody Map<String, Integer> map) {
        Integer uid = map.get("uid");
        Integer lotId = map.get("lotId");
        if (uid == null || lotId == null) {
            return new Data<>(200, "未知id", null);
        }
        if (lotDetailService.deleteLotCollection(uid, lotId)) {
            return new Data<>(200, "success", null);
        } else {
            return new Data<>(200, "取消收藏失败，请稍后再试", null);
        }
    }

    @PostMapping("/judgePayPassword")
    public Data<String> judgePayPassword(@RequestBody Map<String, Object> map) {
        Integer uid = (Integer) map.get("uid");
        String payPassword = (String) map.get("payPassword");
        Data<String> data;
        if (uid == null || payPassword == null) {
            return new Data<>(200, "未知请求", null);
        }
        Integer flag = lotDetailService.payPasswordIsTrue(uid, payPassword);
        switch (flag) {
            case LotDetailService.SUCCESS:
                data = new Data<>(200, "success", null);
                break;
            case LotDetailService.UID_NO:
                data = new Data<>(200, "用户ID不存在", null);
                break;
            case LotDetailService.ERROR:
            default:
                data = new Data<>(200, "支付密码错误", null);
        }
        return data;
    }

    // 更新拍卖价格
    @PutMapping("/updateLotPrice")
    public Data<String> updateLotPrice(@RequestBody Map<String, Integer> map) {
        Integer lotId = map.get("lotId");
        Integer uid = map.get("uid");
        Integer price = map.get("price");
        Data<String> data;
        if (uid == null || lotId == null) {
            return new Data<>(200, "未知id", null);
        }
        int flag = lotDetailService.updatePrice(lotId, uid, price);
        switch (flag) {
            case LotDetailService.SUCCESS:
                data = new Data<>(200, "success", null);
                break;
            case LotDetailService.UID_DUPLICATE:
                data = new Data<>(200, "请等待其他人出价", null);
                break;
            case LotDetailService.PRICE_LOW:
                data = new Data<>(200, "价格不正确，请刷新后重试", null);
                break;
            case LotDetailService.ERROR:
            default:
                data = new Data<>(200, "参拍失败，请稍后再试", null);
        }
        return data;
    }

    // 插入拍卖记录
    @PostMapping("/insertLotRecord")
    public Data<String> insertLotRecord(@RequestBody LotRecord lotRecord) {
        if (lotRecord == null) {
            return new Data<>(200, "未知拍卖记录", null);
        }
        if (lotDetailService.insertLotRecord(lotRecord)) {
            return new Data<>(200, "success", null);
        } else {
            return new Data<>(200, "拍卖纪录插入失败，请联系客服", null);
        }
    }

    /**
     * 更新浏览量
     */
    @GetMapping("/lotLook/{lotId}")
    public void updateLook(@PathVariable(name = "lotId") Integer lotId) {
        lotDetailService.updateLook(lotId);
    }

    /**
     * 通过lotId得到拍品拍卖记录信息
     */
    @GetMapping("/lotRecord/{lotId}")
    public Data<LotRecordData> getLotRecordData(@PathVariable(name = "lotId") Integer lotId, @RequestParam(name = "lastRId", required = false) Integer lastRId, @RequestParam("pageSize") Integer pageSize) {
        List<LotRecordData> lotRecordData = lotDetailService.getLotRecordById(lotId, lastRId, pageSize);
        return new Data<>(200, "success", lotRecordData);
    }

    /**
     * 搜索获得拍品卡片信息
     */
    @GetMapping("/search")
    public Data<LotCardData> search(@RequestParam("keyWord") String keyWord,
                                    @RequestParam(name = "lastLotId", required = false) Integer lastLotId,
                                    @RequestParam("pageSize") Integer pageSize) {
        List<LotCardData> searchLotCardData = lotDetailService.search(keyWord, lastLotId, pageSize);
        return new Data<>(200, "success", searchLotCardData);
    }

}
