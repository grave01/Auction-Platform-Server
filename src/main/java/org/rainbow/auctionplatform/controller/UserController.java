package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.bean.User;
import org.rainbow.auctionplatform.service.UserService;
import org.rainbow.auctionplatform.utils.ImageCompress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    // 用户查询
    @GetMapping("/user/{uid}")
    public Data<User> login(@PathVariable("uid") Integer uid) {
        Data<User> userData;
        User user = userService.selectUserByUid(uid);
        if (user != null) {
            userData = new Data<>(200, "ok", Collections.singletonList(user));
        } else {
            userData = new Data<>(200, "用户id不存在", null);
        }
        return userData;
    }

    // 修改用户信息
    @PutMapping("/user")
    public Data<User> updateUserInfo(@RequestBody User user) {
        Data<User> userData;
        int flag;
        flag = userService.updateUser(user);
        switch (flag) {
            case UserService.UPDATE_SUCCESS:
                userData = new Data<>(200, "ok", Collections.singletonList(user));
                break;
            case UserService.UPDATE_PHONE_DUPLICATE:
                userData = new Data<>(200, "该手机号码已被注册", null);
                break;
            case UserService.UPDATE_EMAIL_DUPLICATE:
                userData = new Data<>(200, "该邮箱已被注册", null);
                break;
            case UserService.UPDATE_QQ_DUPLICATE:
                userData = new Data<>(200, "该QQ已被注册", null);
                break;
            case UserService.UPDATE_PASSWORD_ERROR:
                userData = new Data<>(200, "原登录密码错误", null);
                break;
            case UserService.UPDATE_PAY_PASSWORD_ERROR:
                userData = new Data<>(200, "原支付密码错误", null);
                break;
            case UserService.UPDATE_ERROR:
            default:
                userData = new Data<>(200, "修改异常，请稍后再试", null);
                break;
        }
        return userData;
    }

    // 上传头像
    @PostMapping("/user/photo")
    public Data<String> updatePhoto(@RequestParam("photo") MultipartFile photo,
                              @RequestParam("uid") Integer uid) {
        Data<String> userData;
        int uploadInfo = userService.uploadPhoto(photo, uid);
        switch (uploadInfo) {
            case ImageCompress.IMAGE_COMPRESS_SUCCESS:
                //得到上传时的原文件名
                String originalFilename = photo.getOriginalFilename();
                //获取文件格式
                String ext = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
                userData = new Data<>(200, "ok", Collections.singletonList(uid + "." + ext));
                break;
            case ImageCompress.IMAGE_NO_FILE_ERROR:
                userData = new Data<>(200, "图片文件出错，请稍后再试", null);
                break;
            case ImageCompress.IMAGE_ERROR:
            default:
                userData = new Data<>(200, "上传出错，请稍后再试", null);
                break;
        }
        return userData;
    }

    // 修改密码
    @PutMapping("/user/changePw")
    public Data<String> updatePassword(@RequestBody Map<String, Object> map) {
        Data<String> userData;
        int flag;
        Integer uid = (Integer) map.get("uid");
        String oldPw = (String) map.get("oldPw");
        String newPw = (String) map.get("newPw");
        if (uid == null || oldPw == null || newPw == null) {
            flag = UserService.UPDATE_ERROR;
        } else {
            // 0代表修改登录密码
            flag = userService.updatePassword(uid, oldPw, newPw, 0);
        }
        switch (flag) {
            case UserService.UPDATE_SUCCESS:
                userData = new Data<>(200, "ok", null);
                break;
            case UserService.UPDATE_PASSWORD_ERROR:
                userData = new Data<>(200, "原登录密码错误", null);
                break;
            case UserService.UPDATE_PAY_PASSWORD_ERROR:
                userData = new Data<>(200, "原支付密码错误", null);
                break;
            case UserService.UPDATE_ERROR:
            default:
                userData = new Data<>(200, "修改异常，请稍后再试", null);
                break;
        }
        return userData;
    }

    // 修改密码
    @PutMapping("/user/changePayPw")
    public Data<String> updatePayPassword(@RequestBody Map<String, Object> map) {
        Data<String> userData;
        int flag;
        Integer uid = (Integer) map.get("uid");
        String oldPw = (String) map.get("oldPw");
        String newPw = (String) map.get("newPw");
        if (uid == null || oldPw == null || newPw == null) {
            flag = UserService.UPDATE_ERROR;
        } else {
            // 1代表修改支付密码
            flag = userService.updatePassword(uid, oldPw, newPw, 1);
        }
        switch (flag) {
            case UserService.UPDATE_SUCCESS:
                userData = new Data<>(200, "ok", null);
                break;
            case UserService.UPDATE_PASSWORD_ERROR:
                userData = new Data<>(200, "原登录密码错误", null);
                break;
            case UserService.UPDATE_PAY_PASSWORD_ERROR:
                userData = new Data<>(200, "原支付密码错误", null);
                break;
            case UserService.UPDATE_ERROR:
            default:
                userData = new Data<>(200, "修改异常，请稍后再试", null);
                break;
        }
        return userData;
    }
}
