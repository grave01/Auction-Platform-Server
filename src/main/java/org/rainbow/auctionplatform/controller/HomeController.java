package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.pojo.HomeSliderData;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.rainbow.auctionplatform.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Home页面视图Controller
 */
@RestController
public class HomeController {

    private final HomeService homeService;

    // 构造器式注入
    @Autowired
    public HomeController(HomeService homeService) {
        this.homeService = homeService;
    }

    /**
     * 获取首页轮播图展示的内容
     */
    @GetMapping("/home-slider")
    public Data<HomeSliderData> getHomeSlider() {
        List<HomeSliderData> homeSlider = homeService.getHomeSlider();
        return new Data<>(200, "success", homeSlider);
    }

    /**
     * 获取首页展示拍品卡片的瀑布流
     * @param type 拍品类型
     * @param lastLotId 用于定位从哪里开始加载内容
     */
    @GetMapping("/lots")
    public Data<LotCardData> getLotCardsByType(@RequestParam(name = "type", required = false) Integer type,
                                                           @RequestParam(name = "lastLotId", required = false) Integer lastLotId,
                                                           @RequestParam(name = "pageSize") Integer pageSize) {
        List<LotCardData> lotCardData = homeService.getLotCards(type, lastLotId, pageSize);
        return new Data<>(200, "success", lotCardData);
    }

}
