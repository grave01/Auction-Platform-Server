package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.rainbow.auctionplatform.service.MineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Mine页面视图Controller
 */
@RestController
public class MineController {

    private final MineService mineService;

    @Autowired
    public MineController(MineService mineService) {
        this.mineService = mineService;
    }

    /**
     * 获取我的拍卖拍品卡片的瀑布流
     * @param uid 个人id
     * @param lastLotId 用于定位从哪里开始加载内容
     */
    @GetMapping("/myLots")
    public Data<LotCardData> getMyLotCardsByUid(@RequestParam(name = "uid") Integer uid,
                                                @RequestParam(name = "lastLotId", required = false) Integer lastLotId,
                                                @RequestParam(name = "pageSize") Integer pageSize) {
        List<LotCardData> lotCardData = mineService.getMyLotCards(uid, lastLotId, pageSize);
        return new Data<>(200, "success", lotCardData);
    }

    /**
     * 获取我的收藏拍品卡片的瀑布流
     * @param uid 个人id
     * @param lastCId 用于定位从哪里开始加载内容
     */
    @GetMapping("/myCollectionLots")
    public Data<LotCardData> getMyCollectionLotCardsByUid(@RequestParam(name = "uid") Integer uid,
                                                          @RequestParam(name = "lastCId", required = false) Integer lastCId,
                                                          @RequestParam(name = "pageSize") Integer pageSize) {
        List<LotCardData> lotCardData = mineService.getMyCollectionLotCards(uid, lastCId, pageSize);
        return new Data<>(200, "success", lotCardData);
    }

    /**
     * 获取参与的拍卖卡片的瀑布流
     * @param uid 个人id
     * @param lastLotId 用于定位从哪里开始加载内容
     */
    @GetMapping("/participatingAuctionLots")
    public Data<LotCardData> getParticipatingAuctionsLotCards(@RequestParam(name = "uid") Integer uid,
                                                          @RequestParam(name = "lastLotId", required = false) Integer lastLotId,
                                                          @RequestParam(name = "pageSize") Integer pageSize) {
        List<LotCardData> lotCardData = mineService.getParticipatingAuctionsLotCards(uid, lastLotId, pageSize);
        return new Data<>(200, "success", lotCardData);
    }
}
