package org.rainbow.auctionplatform.controller;

import org.rainbow.auctionplatform.bean.Data;
import org.rainbow.auctionplatform.bean.User;
import org.rainbow.auctionplatform.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class LoginController {
    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public Data<User> login(@RequestBody Map<String,String> map) {
        Data<User> userData;
        if (!map.isEmpty()) {
            User userInfo = loginService.selectUser(map.get("account"), map.get("password"));
            if (userInfo != null) {
                userData = new Data<>(200, "ok", Collections.singletonList(userInfo));
            } else {
                userData = new Data<>(200, "用户名或密码错误", null);
            }
            return userData;
        }
        userData = new Data<>(200, "登陆数据异常，请稍后再试", null);
        return userData;
    }
}
