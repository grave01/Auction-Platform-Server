package org.rainbow.auctionplatform.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class Lot implements Serializable {

    private static final long serialVersionUID = 530487533616524603L;

    // 拍品id
    private Integer lotId;
    // 拍品类型
    private Integer type;
    // 拍品帖子标题
    private String title;
    // 拍品详细信息
    private String info;
    // 发布时间
    private String date;
    // 拍卖结束时间
    private String endDate;
    // 起拍价
    private BigDecimal startPrice;
    // 加价幅度
    private BigDecimal markupRange;
    // 拍品图片地址
    private String images;
    // 拍品标识 0(已拍卖)/1(正在拍卖)/2(流拍)
    private Integer tag;
    // 帖子浏览量
    private Integer look;
    // 拍得的用户id
    private Integer auctionUid;
    // 发布帖子的用户id
    private Integer uid;

    public Lot() {
    }

    public Lot(Integer lotId, Integer type, String title, String info, String date, String endDate, BigDecimal startPrice, BigDecimal markupRange, String images, Integer tag, Integer look, Integer auctionUid, Integer uid) {
        this.lotId = lotId;
        this.type = type;
        this.title = title;
        this.info = info;
        this.date = date;
        this.endDate = endDate;
        this.startPrice = startPrice;
        this.markupRange = markupRange;
        this.images = images;
        this.tag = tag;
        this.look = look;
        this.auctionUid = auctionUid;
        this.uid = uid;
    }

    public Lot(Integer type, String title, String info, String date, String endDate, BigDecimal startPrice, BigDecimal markupRange, String images, Integer tag, Integer look, Integer auctionUid, Integer uid) {
        this.type = type;
        this.title = title;
        this.info = info;
        this.date = date;
        this.endDate = endDate;
        this.startPrice = startPrice;
        this.markupRange = markupRange;
        this.images = images;
        this.tag = tag;
        this.look = look;
        this.auctionUid = auctionUid;
        this.uid = uid;
    }

    public Integer getLotId() {
        return lotId;
    }

    public void setLotId(Integer lotId) {
        this.lotId = lotId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getMarkupRange() {
        return markupRange;
    }

    public void setMarkupRange(BigDecimal markupRange) {
        this.markupRange = markupRange;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Integer getLook() {
        return look;
    }

    public void setLook(Integer look) {
        this.look = look;
    }

    public Integer getAuctionUid() {
        return auctionUid;
    }

    public void setAuctionUid(Integer auctionUid) {
        this.auctionUid = auctionUid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "lotId=" + lotId +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", info='" + info + '\'' +
                ", date='" + date + '\'' +
                ", endDate='" + endDate + '\'' +
                ", startPrice=" + startPrice +
                ", markupRange=" + markupRange +
                ", images='" + images + '\'' +
                ", tag=" + tag +
                ", look=" + look +
                ", auctionUid=" + auctionUid +
                ", uid=" + uid +
                '}';
    }
}
