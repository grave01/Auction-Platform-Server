package org.rainbow.auctionplatform.bean;

import java.io.Serializable;
import java.util.List;

public class Data<T> implements Serializable {

    private static final long serialVersionUID = -6611391197097682486L;

    // 状态码
    private Integer statusCode;
    // 信息
    private String msg;
    // 数据
    private List<T> data;

    public Data() {
    }

    public Data(Integer statusCode, String msg, List<T> data) {
        this.statusCode = statusCode;
        this.msg = msg;
        this.data = data;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Data{" +
                "statusCode=" + statusCode +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
