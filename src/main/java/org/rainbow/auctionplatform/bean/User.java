package org.rainbow.auctionplatform.bean;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = -393978092736518814L;

    // 用户id
    private Integer uid;
    // 用户昵称
    private String name;
    // 用户密码
    private String password;
    // 用户支付密码
    private String payPassword;
    // 用户电话
    private String phone;
    // 用户邮箱
    private String email;
    // 用户性别
    private Integer gender;
    // 用户QQ
    private String qq;
    // 用户头像地址
    private String photo;

    public User() {
    }

    public User(String name, String password, String payPassword, String phone, String email, Integer gender, String qq, String photo) {
        this.name = name;
        this.password = password;
        this.payPassword = payPassword;
        this.phone = phone;
        this.email = email;
        this.gender = gender;
        this.qq = qq;
        this.photo = photo;
    }

    public User(Integer uid, String name, String password, String payPassword, String phone, String email, Integer gender, String qq, String photo) {
        this.uid = uid;
        this.name = name;
        this.password = password;
        this.payPassword = payPassword;
        this.phone = phone;
        this.email = email;
        this.gender = gender;
        this.qq = qq;
        this.photo = photo;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", payPassword='" + payPassword + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", gender=" + gender +
                ", qq='" + qq + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}
