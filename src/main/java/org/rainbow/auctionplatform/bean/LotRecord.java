package org.rainbow.auctionplatform.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class LotRecord implements Serializable{

    private static final long serialVersionUID = -8901638015247115634L;

    // 拍品记录唯一标识id
    private Integer cId;
    // 参拍时间
    private String date;
    // 参拍价格
    private BigDecimal price;
    // 参拍人id
    private Integer uid;
    // 参拍拍品id
    private Integer lotId;

    public LotRecord() {
    }

    public LotRecord(Integer cId, String date, BigDecimal price, Integer uid, Integer lotId) {
        this.cId = cId;
        this.date = date;
        this.price = price;
        this.uid = uid;
        this.lotId = lotId;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getLotId() {
        return lotId;
    }

    public void setLotId(Integer lotId) {
        this.lotId = lotId;
    }

    @Override
    public String toString() {
        return "LotRecord{" +
                "cId=" + cId +
                ", date='" + date + '\'' +
                ", price=" + price +
                ", uid=" + uid +
                ", lotId=" + lotId +
                '}';
    }
}
