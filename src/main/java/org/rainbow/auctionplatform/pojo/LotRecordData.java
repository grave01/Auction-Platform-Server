package org.rainbow.auctionplatform.pojo;

import java.math.BigDecimal;

public class LotRecordData {
    // 拍卖记录id
    private Integer rId;
    // 出价时间
    private String date;
    // 价格
    private BigDecimal price;
    // 用户id
    private Integer uid;

    public LotRecordData() {
    }

    public LotRecordData(Integer rId, String date, BigDecimal price, Integer uid) {
        this.rId = rId;
        this.date = date;
        this.price = price;
        this.uid = uid;
    }

    public Integer getrId() {
        return rId;
    }

    public void setrId(Integer rId) {
        this.rId = rId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "LotRecordData{" +
                "rId=" + rId +
                ", date='" + date + '\'' +
                ", price=" + price +
                ", uid=" + uid +
                '}';
    }
}
