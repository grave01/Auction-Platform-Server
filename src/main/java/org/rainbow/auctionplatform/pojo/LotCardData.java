package org.rainbow.auctionplatform.pojo;

// 拍品卡片展示数据基类
public class LotCardData {

    // 拍品ID
    private Integer lotId;
    // 用户ID
    private Integer uid;
    // 用户头像地址
    private String photo;
    // 用户昵称
    private String name;
    // 发帖日期
    private String date;
    // 标题
    private String title;
    // 信息
    private String info;
    // 图片地址
    private String images;
    // 拍品标识 0(已拍卖)/1(正在拍卖)/2(流拍)
    private Integer tag;
    // 浏览量
    private Integer look;

    public LotCardData() {
    }

    public LotCardData(Integer lotId, Integer uid, String photo, String name, String date, String title, String info, String images, Integer tag, Integer look) {
        this.lotId = lotId;
        this.uid = uid;
        this.photo = photo;
        this.name = name;
        this.date = date;
        this.title = title;
        this.info = info;
        this.images = images;
        this.tag = tag;
        this.look = look;
    }

    public Integer getLotId() {
        return lotId;
    }

    public void setLotId(Integer lotId) {
        this.lotId = lotId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Integer getLook() {
        return look;
    }

    public void setLook(Integer look) {
        this.look = look;
    }

    @Override
    public String toString() {
        return "LotCardData{" +
                "lotId=" + lotId +
                ", uid=" + uid +
                ", photo='" + photo + '\'' +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", title='" + title + '\'' +
                ", info='" + info + '\'' +
                ", images='" + images + '\'' +
                ", tag=" + tag +
                ", look=" + look +
                '}';
    }
}
