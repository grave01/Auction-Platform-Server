package org.rainbow.auctionplatform.pojo;

import java.math.BigDecimal;
import java.util.List;

public class LotDetailData {
    // 拍品ID
    private Integer lotId;

    // 拍品类型
    private Integer type;

    // 标题
    private String title;

    // 详情
    private String info;

    // 发布日期
    private String date;

    // 拍卖结束时间
    private String endDate;

    // 起拍价
    private BigDecimal startPrice;

    // 加价幅度
    private BigDecimal markupRange;

    // 拍品图片地址
    private String images;

    // 拍品标识 0(已拍卖)/1(正在拍卖)/2(流拍)
    private Integer tag;

    // 浏览量
    private Integer look;

    // 收藏量
    private Integer collectionNum;

    // 拍得的用户id
    private Integer auctionUid;

    // 发布帖子的用户id
    private Integer uid;

    // 发帖人昵称
    private String name;

    // 发帖人头像地址
    private String photo;

    // 拍卖记录
    private List<LotRecordData> lotRecordData;

    public LotDetailData() {
    }

    public LotDetailData(Integer lotId, Integer type, String title, String info, String date, String endDate, BigDecimal startPrice, BigDecimal markupRange, String images, Integer tag, Integer look, Integer collectionNum, Integer auctionUid, Integer uid, String name, String photo, List<LotRecordData> lotRecordData) {
        this.lotId = lotId;
        this.type = type;
        this.title = title;
        this.info = info;
        this.date = date;
        this.endDate = endDate;
        this.startPrice = startPrice;
        this.markupRange = markupRange;
        this.images = images;
        this.tag = tag;
        this.look = look;
        this.collectionNum = collectionNum;
        this.auctionUid = auctionUid;
        this.uid = uid;
        this.name = name;
        this.photo = photo;
        this.lotRecordData = lotRecordData;
    }

    public Integer getLotId() {
        return lotId;
    }

    public void setLotId(Integer lotId) {
        this.lotId = lotId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getMarkupRange() {
        return markupRange;
    }

    public void setMarkupRange(BigDecimal markupRange) {
        this.markupRange = markupRange;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Integer getLook() {
        return look;
    }

    public void setLook(Integer look) {
        this.look = look;
    }

    public Integer getCollectionNum() {
        return collectionNum;
    }

    public void setCollectionNum(Integer collectionNum) {
        this.collectionNum = collectionNum;
    }

    public Integer getAuctionUid() {
        return auctionUid;
    }

    public void setAuctionUid(Integer auctionUid) {
        this.auctionUid = auctionUid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<LotRecordData> getLotRecordData() {
        return lotRecordData;
    }

    public void setLotRecordData(List<LotRecordData> lotRecordData) {
        this.lotRecordData = lotRecordData;
    }

    @Override
    public String toString() {
        return "LotDetailData{" +
                "lotId=" + lotId +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", info='" + info + '\'' +
                ", date='" + date + '\'' +
                ", endDate='" + endDate + '\'' +
                ", startPrice=" + startPrice +
                ", markupRange=" + markupRange +
                ", images='" + images + '\'' +
                ", tag=" + tag +
                ", look=" + look +
                ", collectionNum=" + collectionNum +
                ", auctionUid=" + auctionUid +
                ", uid=" + uid +
                ", name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}

