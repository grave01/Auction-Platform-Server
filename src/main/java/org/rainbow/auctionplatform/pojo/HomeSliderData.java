package org.rainbow.auctionplatform.pojo;

public class HomeSliderData {
    // 拍品ID
    private Integer lotId;

    // 图片地址(1张)
    private String imageUrl;

    public HomeSliderData() {
    }

    public HomeSliderData(Integer lotId, String imageUrl) {
        this.lotId = lotId;
        this.imageUrl = imageUrl;
    }

    public Integer getLotId() {
        return lotId;
    }

    public void setLotId(Integer lotId) {
        this.lotId = lotId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "HomeSliderData{" +
                "lotId=" + lotId +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}

