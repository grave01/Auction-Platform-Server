package org.rainbow.auctionplatform.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.rainbow.auctionplatform.dao.QuartzJobMapper;
import org.rainbow.auctionplatform.service.LotDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuctionEndedJob implements Job {

    @Autowired
    private QuartzJobMapper quartzJobMapper;

    @Autowired
    private LotDetailService lotDetailService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        String jobName = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("jobName");
        // 拍卖时间到了之后自动更新tag
        lotDetailService.updateTag(Integer.valueOf(jobName));
        quartzJobMapper.deleteJob(jobName);
    }
}
