package org.rainbow.auctionplatform.listener;


import org.quartz.SchedulerException;
import org.rainbow.auctionplatform.utils.QuartzJobManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

// tomcat启动时从数据库读取所有的任务
@Component("StartupListener")
public class InitSystemJobsListener implements ApplicationListener<ContextRefreshedEvent> {
    private int runTime = 0;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        runTime++;
        if (2 == runTime) {
            // 获取spring管理的Bean
            ApplicationContext context = event.getApplicationContext();
            QuartzJobManager quartzJobManager = (QuartzJobManager) context.getBean("quartzJobManager");
            try {
                quartzJobManager.loadJobs();
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
    }

}