package org.rainbow.auctionplatform.service;

import org.rainbow.auctionplatform.bean.User;
import org.rainbow.auctionplatform.dao.UserMapper;
import org.rainbow.auctionplatform.properties.UploadFileProperties;
import org.rainbow.auctionplatform.utils.ImageCompress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Service
public class UserService {

    // 修改成功
    public final static int UPDATE_SUCCESS = 0;
    // 电话号码重复
    public final static int UPDATE_PHONE_DUPLICATE = -100;
    // 邮箱重复
    public final static int UPDATE_EMAIL_DUPLICATE = -200;
    // QQ重复
    public final static int UPDATE_QQ_DUPLICATE = -300;
    // 登录密码不正确
    public final static int UPDATE_PASSWORD_ERROR = -400;
    // 支付密码不正确
    public final static int UPDATE_PAY_PASSWORD_ERROR = -500;
    // 修改失败的其他错误
    public final static int UPDATE_ERROR = -1;

    @Resource
    UploadFileProperties uploadFileProperties;

    private final UserMapper userMapper;

    @Autowired
    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    // 通过id查询用户信息
    public User selectUserByUid(Integer uid) {
        return userMapper.selectUserByUid(uid);
    }

    // 修改用户信息（不包含密码）
    public int updateUser(User user) {
        // 修改成功标识
        Boolean flag;
        // 修改错误消息
        String error_msg = "";
        try {
            flag = userMapper.updateUser(user);
        } catch (Exception e) {
            error_msg = e.getCause().getMessage();
            flag = false;
        }
        if (flag) {
            return UPDATE_SUCCESS;
        } else {
            if (error_msg.contains("Duplicate entry")) {
                if (error_msg.contains("phone")) {
                    return UPDATE_PHONE_DUPLICATE;
                } else if (error_msg.contains("email")) {
                    return UPDATE_EMAIL_DUPLICATE;
                } else if (error_msg.contains("qq")) {
                    return UPDATE_QQ_DUPLICATE;
                }
            }
            return UPDATE_ERROR;
        }
    }

    // 修改头像
    public int uploadPhoto(MultipartFile photo, Integer uid) {
        int flag = ImageCompress.compressFile(photo, uid.toString(), uploadFileProperties.getPhotoPath());
        if (flag == ImageCompress.IMAGE_COMPRESS_SUCCESS) {
            //得到上传时的原文件名
            String originalFilename = photo.getOriginalFilename();
            //获取文件格式
            String ext = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
            return uploadPhotoPath(uid, uid + "." + ext);
        } else {
            return flag;
        }
    }

    // 修改数据可头像地址
    public int uploadPhotoPath(Integer uid, String photo) {
        // 修改成功标识
        Boolean flag;
        try {
            flag = userMapper.updateUserPhoto(uid, photo);
        } catch (Exception e) {
            flag = false;
        }
        if (flag) {
            return UPDATE_SUCCESS;
        } else {
            return UPDATE_ERROR;
        }
    }

    // 修改用户密码
    public int updatePassword(Integer uid, String oldPw, String newPw, int type) {
        // 修改成功标识
        Boolean flag;
        // 修改错误消息
        String error_msg = "";
        User userInfo = userMapper.selectUserAndPwByUid(uid);
        if (userInfo != null) {
            // uid正确
            if (type == 0) {
                // 修改的是登录密码
                if (userInfo.getPassword().equals(oldPw)) {
                    // 原密码正确
                    try {
                        flag = userMapper.updatePassword(uid, newPw);
                    } catch (Exception e) {
                        error_msg = e.getCause().getMessage();
                        flag = false;
                    }
                } else {
                    // 原密码错误
                    error_msg = "old login password error";
                    flag = false;
                }
            } else if (type == 1) {
                // 修改的是支付密码
                if (userInfo.getPayPassword().equals(oldPw)) {
                    // 原密码正确
                    try {
                        flag = userMapper.updatePayPassword(uid, newPw);
                    } catch (Exception e) {
                        error_msg = e.getCause().getMessage();
                        flag = false;
                    }
                } else {
                    // 原密码错误
                    error_msg = "old pay password error";
                    flag = false;
                }
            } else {
                flag = false;
            }
        } else {
            flag = false;
        }
        if (flag) {
            return UPDATE_SUCCESS;
        } else {
            if (error_msg.contains("password error")) {
                if (error_msg.contains("login")) {
                    return UPDATE_PASSWORD_ERROR;
                } else if (error_msg.contains("pay")) {
                    return UPDATE_PAY_PASSWORD_ERROR;
                }
            }
            return UPDATE_ERROR;
        }
    }
}
