package org.rainbow.auctionplatform.service;

import org.rainbow.auctionplatform.dao.LotMapper;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MineService {

    private final LotMapper lotMapper;

    @Autowired
    public MineService(LotMapper lotMapper) {
        this.lotMapper = lotMapper;
    }

    /**
     * 得到我的拍品卡片数据
     * @param uid 个人id
     * @param lastLotId 当前最后一个lotId，用于定位从哪里开始加载
     * @param pageSize 每页拍品数量
     */
    public List<LotCardData> getMyLotCards(Integer uid, Integer lastLotId, Integer pageSize) {
        return lotMapper.getMyLotCards(uid, lastLotId, pageSize);
    }

    /**
     * 得到我的拍品卡片数据
     * @param uid 个人id
     * @param lastCId 当前最后一个cId，用于定位从哪里开始加载
     * @param pageSize 每页拍品数量
     */
    public List<LotCardData> getMyCollectionLotCards(Integer uid, Integer lastCId, Integer pageSize) {
        return lotMapper.getMyCollectionLotCards(uid, lastCId, pageSize);
    }

    /**
     * 得到参与的拍卖卡片数据
     * @param uid 个人id
     * @param lastLotId 当前最后一个rId，用于定位从哪里开始加载
     * @param pageSize 每页拍品数量
     */
    public List<LotCardData> getParticipatingAuctionsLotCards(Integer uid, Integer lastLotId, Integer pageSize) {
        return lotMapper.getParticipatingAuctionsLotCards(uid, lastLotId, pageSize);
    }
}
