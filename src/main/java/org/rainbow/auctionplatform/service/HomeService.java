package org.rainbow.auctionplatform.service;

import org.rainbow.auctionplatform.dao.LotMapper;
import org.rainbow.auctionplatform.pojo.HomeSliderData;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 首页Service服务层
 */
@Service
public class HomeService {

    private final LotMapper lotMapper;

    @Autowired
    public HomeService(LotMapper lotMapper) {
        this.lotMapper = lotMapper;
    }

    /**
     * 获取轮播图内容
     */
    public List<HomeSliderData> getHomeSlider() {
        return lotMapper.getHomeSlider();
    }

    /**
     * 得到拍品卡片数据
     * @param type 拍品类型
     * @param lastLotId 当前最后一个lotId，用于定位从哪里开始加载
     * @param pageSize 每页拍品数量
     */
    public List<LotCardData> getLotCards(Integer type, Integer lastLotId, Integer pageSize) {
        return lotMapper.getLotCards(type, lastLotId, pageSize);
    }
}
