package org.rainbow.auctionplatform.service;

import org.rainbow.auctionplatform.bean.LotRecord;
import org.rainbow.auctionplatform.dao.LotMapper;
import org.rainbow.auctionplatform.pojo.LotCardData;
import org.rainbow.auctionplatform.pojo.LotDetailData;
import org.rainbow.auctionplatform.pojo.LotRecordData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class LotDetailService {

    // 成功
    public final static int SUCCESS = 0;
    // 更新失败
    public final static int ERROR = -1;
    // 同一人联系拍卖错误
    public final static int UID_DUPLICATE = -100;
    // uid不存在
    public final static int UID_NO = -200;
    // 价格低
    public final static int PRICE_LOW = -300;

    private final LotMapper lotMapper;

    @Autowired
    public LotDetailService(LotMapper lotMapper) {
        this.lotMapper = lotMapper;
    }

    /**
     * 通过lotId得到拍品信息
     * @param lotId 拍品id
     * @return 拍品信息
     */
    public LotDetailData getLotById(Integer lotId) {
        LotDetailData lotDetailData = lotMapper.getLotById(lotId);
        lotDetailData.setCollectionNum(lotMapper.getLotRecordNum(lotId));
        lotDetailData.setLotRecordData(lotMapper.getLotRecord(lotId, null, 3));
        return lotDetailData;
    }


    /**
     * 查询是否收藏
     * @param uid 用户id
     * @param lotId 拍品id
     * @return 是否收藏
     */
    public Boolean getIsCollection(Integer uid, Integer lotId) {
        return lotMapper.getIsCollection(uid, lotId);
    }

    /**
     * 插入收藏
     * @param uid 用户id
     * @param lotId 拍品id
     * @return 插入是否成功
     */
    public Boolean insertLotCollection(Integer uid, Integer lotId) {
        Boolean flag;
        try {
            flag = lotMapper.insertLotCollection(uid, lotId);
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
            resetMaxUid();
        }
        return flag;
    }

    /**
     * 删除收藏
     * @param uid 用户id
     * @param lotId 拍品id
     * @return 删除信息
     */
    public Boolean deleteLotCollection(Integer uid, Integer lotId) {
        Boolean flag;
        try {
            flag = lotMapper.deleteLotCollection(uid, lotId);
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
        }
        resetMaxUid();
        return flag;
    }

    /**
     * 通过lotId得到拍品记录信息
     * @param lotId 拍品id
     * @return 拍品记录
     */
    public List<LotRecordData> getLotRecordById(Integer lotId, Integer lastRId, Integer pageSize) {
        return lotMapper.getLotRecord(lotId, lastRId, pageSize);
    }


    /**
     * 判断支付密码是否正确
     * @param uid 用户uid
     * @param payPassword 用户支付密码
     * @return 是否正确
     */
    public Integer payPasswordIsTrue (Integer uid, String payPassword) {
        String s_PayPassword = lotMapper.selectPayPassword(uid);
        if (s_PayPassword == null) {
            // 用户uid不存在
            return UID_NO;
        }
        if (s_PayPassword.equals(payPassword)) {
            // 支付密码正确
            return SUCCESS;
        }
        // 支付密码错误
        return ERROR;
    }

    /**
     * 更新拍卖价格
     * @param lotId 拍品id
     * @param uid 拍卖的用户id
     */
    public int updatePrice(Integer lotId, Integer uid, Integer price) {
        Boolean flag;
        try {
            // 得到最后一条拍卖记录
            LotRecord lastLotRecord = lotMapper.getLastLotRecord(lotId);
            if (lastLotRecord != null && lastLotRecord.getUid().equals(uid)) {
                // 最后一条拍卖也是自己
                return UID_DUPLICATE;
            }
            LotDetailData lot = lotMapper.getLotById(lotId);
            BigDecimal lastPrice;
            // 得到最后拍卖价格
            if (lastLotRecord == null || lastLotRecord.getPrice() == null) {
                lastPrice = lot.getStartPrice();
            } else {
                lastPrice = lastLotRecord.getPrice();
            }
            // 得到加价幅度
            BigDecimal markupRange = lot.getMarkupRange();
            // 拍卖价格
            BigDecimal nowPrice = new BigDecimal(price);
            if (lastPrice.add(markupRange).compareTo(nowPrice) > 0) {
                // 价格低
                return PRICE_LOW;
            }
            flag = lotMapper.updatePrice(lotId, uid);
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
        }
        if (flag) {
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    /**
     * 插入拍卖记录
     * @param lotRecord 拍卖记录
     * @return 插入信息
     */
    public Boolean insertLotRecord(LotRecord lotRecord) {
        Boolean flag;
        try {
            flag = lotMapper.insertLotRecord(lotRecord);
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
            resetMaxUid();
        }
        return flag;
    }

    /**
     * 更新浏览量
     * @param lotId 拍品id
     */
    public void updateLook(Integer lotId) {
        lotMapper.updateLook(lotId);
    }

    /**
     * 更新tag
     * @param lotId 拍品id
     */
    public void updateTag(Integer lotId) {
        String uid = lotMapper.selectAuctionUid(lotId);
        int tag;
        if (uid == null) {
            tag = 2;
        } else {
            tag = 0;
        }
        lotMapper.updateTag(lotId, tag);
    }

    /**
     * 搜索
     * @param keyWord 关键词
     * @param lastLotId 最后的拍品id
     * @param pageSize 每页个数
     * @return 拍品集合
     */
    public List<LotCardData> search(String keyWord, Integer lastLotId, Integer pageSize) {
        return lotMapper.search(keyWord, lastLotId, pageSize);
    }

    /**
     * 重置自增id缓存
     * mysql的自增会在插入失败事务回滚时继续自增，所以需要在回滚后先查出实际的最大uid
     * 然后把缓存中的自增uid重置为最大uid+1
     */
    public void resetMaxUid() {
        int maxId;
        try {
            maxId = lotMapper.selectMaxUid();
        } catch (Exception e) {
            maxId = 10000;
        }
        if (maxId != 0) {
            lotMapper.resetMaxUid(maxId + 1);
        }
    }
}
