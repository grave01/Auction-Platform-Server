package org.rainbow.auctionplatform.service;

import org.rainbow.auctionplatform.bean.User;
import org.rainbow.auctionplatform.dao.LoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    private final LoginMapper loginMapper;

    @Autowired
    public LoginService(LoginMapper loginMapper) {
        this.loginMapper = loginMapper;
    }
    // 查询用户
    public User selectUser(String account, String password) {
        return loginMapper.selectUser(account, password);
    }
}
